$(document).ready(function() {
    App.init();
});

App = {
    web3Provider: null,
    contracts: {},
    loading: false,
    init: function() {
        return App.initWeb3();
    },
    initWeb3: function() {
        // initialize web3
        if(typeof web3 !== 'undefined') {
            //reuse the provider of the Web3 object injected by Metamask
            App.web3Provider = web3.currentProvider;
            App.account = web3.eth.accounts[0];
        } else {
            //create a new provider and plug it directly into our local node
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:9545');
        }
        web3 = new Web3(App.web3Provider);
        return App.initContract();
    },

    initContract: function() {
        $.getJSON('../../build/contracts/SNCoin.json', function(chainListArtifact) {
            // get the contract artifact file and use it to instantiate a truffle contract abstraction
            App.contracts.SNCoin = TruffleContract(chainListArtifact);
            // set the provider for our contracts
            App.contracts.SNCoin.setProvider(App.web3Provider);
        });
    },
    /*insertCarData: function(vin_code, last_oil_change, last_service_visit, kms, year, country, color)  {
        App.contracts.CarData.deployed().then(function(instance) {
            return instance.insertCarData(vin_code, last_oil_change, last_service_visit, kms, year, country, color, {
                from: App.account,
                gas: 200000
            });
        }).then(function(result) {
            App.events.logCarDataInserted();
        }).catch(function(err) {
            console.error(err);
            bootbox.alert("Something went wrong.");
        });
    },*/
    getSNCoin: function()  {
        App.contracts.SNCoin.deployed().then(function(instance) {
            return instance.getOwner.call();
        }).then(function(result) {
            console.log(result);
        }).catch(function(err) {
            console.error(err);
        });
    },
    events: {
        /*logCarDataInserted: function() {
            App.contracts.CarData.deployed().then(function(instance) {
                return instance.logCarDataInserted({}, {}).watch(function(error, event) {
                    if (!error) {
                        console.log(event);
                    } else {
                        console.error(error);
                    }
                });
            });
        }*/
    }
};