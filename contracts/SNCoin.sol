pragma solidity 0.4.24;

import "./BaseContract.sol";

//Token based on ERC20 standard
contract SNCoin is BaseContract {
    //=================================STATE=====================================
    address owner;
    uint256 constant private max_uint256 = 2**256 - 1;
    mapping (address => uint256) public balances;
    mapping (address => mapping (address => uint256)) public allowed;
    uint256 public totalSupply;
    string public name;
    uint8 public decimals;
    string public symbol;

    constructor(uint256 _initialAmount, string _tokenName, uint8 _decimalUnits, string _tokenSymbol) public payable {
        owner = msg.sender;
        balances[msg.sender] = _initialAmount;               // Give the creator all initial tokens
        totalSupply = _initialAmount;                        // Update total supply
        name = _tokenName;                                   // Set the name for display purposes
        decimals = _decimalUnits;                            // Amount of decimals for display purposes
        symbol = _tokenSymbol;                               // Set the symbol for display purposes
    }

    //=================================EVENTS=====================================


    //=================================MODIFIERS=====================================
    modifier isOwner {
        require(msg.sender == owner);
        _;
    }

    //=================================BODY=====================================

    //transfering to address from msg.sender
    function transfer(address _to, uint256 _value) public returns (bool success) {
        require(balances[msg.sender] >= _value);
        balances[msg.sender] -= _value;
        balances[_to] += _value;
        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    //msg.sender must be allowed and he can send tokens from _from balance to where ever he wants
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        uint256 allowance = allowed[_from][msg.sender];
        require(_value != 0 && balances[_from] >= _value && allowance >= _value);
        balances[_to] += _value;
        balances[_from] -= _value;
        if (allowance < max_uint256) {
            allowed[_from][msg.sender] -= _value;
        }
        emit Transfer(_from, _to, _value);
        return true;
    }

    //returning balance of passed owner
    function balanceOf(address _owner) isOwner public view returns (uint256 balance) {
        return balances[_owner];
    }

    //approve _spender to spent some amount of tokens of msg.sender balance
    function approve(address _spender, uint256 _value) public returns (bool success) {
        require(_value != 0 && balances[msg.sender] >= _value);
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    //returns how many tokens the spender can spent from owners balance
    function allowance(address _owner, address _spender) public view returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }
}