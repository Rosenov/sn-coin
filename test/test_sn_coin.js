var SNCoin = artifacts.require("./SNCoin.sol");

contract("SNCoin", function(accounts) {
    //TESTING transfer
    it("should send coin correctly", function() {
        var meta;

        //    Get initial balances of first and second account.
        var account_one = accounts[0];
        var account_two = accounts[1];

        var account_one_starting_balance;
        var account_two_starting_balance;
        var account_one_ending_balance;
        var account_two_ending_balance;
        console.log(account_one_starting_balance, account_two_starting_balance, account_one_ending_balance, account_two_ending_balance);

        var amount = 12;
        return SNCoin.deployed().then(function(instance) {
            meta = instance;
            return meta.balanceOf.call(account_one);
        }).then(function(balance) {
            account_one_starting_balance = balance.toNumber();
            return meta.balanceOf.call(account_two);
        }).then(function(balance) {
            account_two_starting_balance = balance.toNumber();
            return meta.transfer(account_two, amount, {from: account_one});
        }).then(function() {
            return meta.balanceOf.call(account_one);
        }).then(function(balance) {
            account_one_ending_balance = balance.toNumber();
            return meta.balanceOf.call(account_two);
        }).then(function(balance) {
            account_two_ending_balance = balance.toNumber();

            console.log(account_one_starting_balance, account_two_starting_balance, account_one_ending_balance, account_two_ending_balance);

            assert.equal(account_one_ending_balance, account_one_starting_balance - amount, "Amount wasn't correctly taken from the sender");
            assert.equal(account_two_ending_balance, account_two_starting_balance + amount, "Amount wasn't correctly sent to the receiver");
        });
    });


    //TESTING approve, transferFrom
    it("should _spender to spent some amount of tokens of msg.sender balance correctly", function() {
        var meta;

        //    Get initial balances of first and second account.
        var account_one = accounts[0];
        var account_two = accounts[1];

        var account_one_starting_balance;
        var account_two_starting_balance;
        var account_one_ending_balance;
        var account_two_ending_balance;
        console.log(account_one_starting_balance, account_two_starting_balance, account_one_ending_balance, account_two_ending_balance);

        var amount = 100;
        return SNCoin.deployed().then(function(instance) {
            meta = instance;
            return meta.balanceOf.call(account_one);
        }).then(function(balance) {
            account_one_starting_balance = balance.toNumber();
            return meta.balanceOf.call(account_two);
        }).then(function(balance) {
            account_two_starting_balance = balance.toNumber();
            return meta.approve(account_two, amount, {from: account_one});
        }).then(function() {
            return meta.transferFrom(account_one, account_two, amount, {from: account_two});
        }).then(function() {
            return meta.balanceOf.call(account_one);
        }).then(function(balance) {
            account_one_ending_balance = balance.toNumber();
            return meta.balanceOf.call(account_two);
        }).then(function(balance) {
            account_two_ending_balance = balance.toNumber();

            console.log(account_one_starting_balance, account_two_starting_balance, account_one_ending_balance, account_two_ending_balance);

            assert.equal(account_one_ending_balance, account_one_starting_balance - amount, "Amount wasn't correctly taken from the ALLOWED sender");
            assert.equal(account_two_ending_balance, account_two_starting_balance + amount, "Amount wasn't correctly sent to the receiver");
        });
    });
});